---
---
---Ngr singleton object holder
---
--- Copyright (c) 2016-2018 www.mwee.cn & Jacobs Lei 
--- Author: Jacobs Lei
--- Date: 2018/7/3
--- Time: 下午3:33

---
-- @field worker_events
--
local _M = {
    loaded_plugins = nil,
    worker_events = nil,
    healthcheckers = nil,
}

return _M